import sys, base64, subprocess, io, tarfile

# Arg parsing
if len(sys.argv) < 2:
    print("usage: decode.py <filename> <password>")
    sys.exit(1)

filename = sys.argv[1]
password = sys.argv[2] if len(sys.argv) >= 3 else None

# Read the file itself
lines = open(filename).readlines()

# Print (some) leading mail info
for line in lines:
    if line.startswith("Date:") or line.startswith("From:") or line.startswith("To:") or line.startswith("Subject:"):
        print(line.strip())
    if not line.strip():
        break
print()

# Extract payload lines (minus whitespace)
payload_start_line = lines.index("xbtoa Begin\n") + 1
payload_lines = [l.strip() for l in lines[payload_start_line:-1]]

# Convert to bytes
payload_encrypted = base64.a85decode("".join(payload_lines))

def is_file_valid(bs):
    # primitive header check... :)
    return bs[0:3] == b"\x1f\x9d\x90"

# Ask for password if we need one
if not password and not is_file_valid(payload_encrypted):
    # We can usually extract the first three characters from header
    password_prefix = chr(payload_encrypted[0] ^ 0x1f) + chr(payload_encrypted[1] ^ 0x9d) + chr(payload_encrypted[2] ^ 0x90)

    print("- File requires password (likely starts with '{}'):".format(password_prefix))
    password = input("> ").strip()

# Attempt to decrypt payload
if password:
    # password is the given key + nul terminator
    cryptkey = password.encode() + b"\x00"

    payload = bytearray(payload_encrypted)
    for i in range(len(payload_encrypted)):
        payload[i] = payload_encrypted[i] ^ cryptkey[i % len(cryptkey)]
    payload = bytes(payload)

    # Confirm valid(ish) password
    if not is_file_valid(payload):
        print("error: invalid password or corrupted archive")
        sys.exit(1)
else:
    # Don't need one at all
    payload = payload_encrypted


print("extracting...", flush=True)

# lzw is so outdated Python doesn't support it (without external libraries)
# so just pipe to system's tar; this may not work everywhere
child = subprocess.Popen(["tar", "-Zxvf", "-"], stdin=subprocess.PIPE)
child.communicate(input=payload)

if child.returncode == 0:
    print("success! files extracted.")
else:
    print("error: invalid password or corrupted archive")

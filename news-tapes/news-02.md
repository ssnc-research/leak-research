# NEWS_02
The `NEWS_02` folder mostly contains encrypted e-mail attachments for various users.

## File layout


- Mails are stored in standard SMTP format, all containing a single Base85/btoa-encoded tarball
- Most of these are "encrypted" with a simple password, using cycling NUL-terminated XOR
- The password can be found in the LOGDATA files for most of them, although mails from `kakui` aren't (I brute-forced those, pretty simple)
- Since the first 3 bytes are a .tar.Z header (1f 9d 90) we can get the first 3 characters of the password (this is why XOR is not good encryption)
- I made a lil Python script to do... most of this work, see [decode-mail.py](./decode-mail.py)
- Numbered ROMs (eg. NU803-0/1.COM) just need to be concatenated to run

## E-mail list

| Path | Date | Subject | Password | Contents | Description |
| --- | --- | --- | --- | --- | --- |
| `usr01/eng/Mail/inbox/1` | Wed, 10 Feb 93 17:05:35 JST | sromchk.lzh  To:Mr.Nishizawa From:Katoh | `nishi` | `SROMCHK.EXE` | A DOS executable |
| `usr01/eng/Mail/inbox/2` | Fri, 12 Mar 93 15:48:04 JST | send0312.lzh  To:Mr.Kiyuna  From:Terasaki | `is` | `SGE.ENV`<br>`SGE.EXE`<br>`SGE.OVR`<br>`SME.EXE`<br>`SME.OVR`<br>`SWM.EXE`<br>`SWM.OVR` | SNES tools ("SNES Gakufu Editor", "SNES Music Editor", "SNES Sound Wave Maker") |
| `usr01/eng/Mail/inbox/3` | Fri, 12 Mar 93 15:48:04 JST | is_file | `isw` | `ISW.COM`<br>`ISW.EXE` | DOS GUI binary, unknown purpose? |
| `usr01/eng/Mail/inbox/4` | Fri, 12 Mar 93 15:48:04 JST | iswn0318.lzh  To:Mr.kiyuna  From:Terasaki | `is` | `IS65.EXE`<br>`ISLINK.EXE`<br>`ISSND.EXE`<br>`ISW.COM`<br>`ISW.EXE`<br>`ISWASM.BAT`<br>`ISWEDIT.BAT`<br>`ISWREQ.COM`<br>`TEST1.X65`<br>`TEST2.X65`<br>`TEST3.X65` | Intelligent Systems toolchain for DOS, plus update of the above ISW |
| `usr01/eng/Mail/inbox/5` | Fri, 19 Mar 93 15:43:19 JST | MARIO_CAD_DATA | `newscad` | `CHIJO.COL`<br>`M-POSE.CGX`<br>`M-POSE.OBJ`<br>`RUN.OBJ`<br>`YOSHI.CGX`<br>`YOSHI.OBJ` | SNES spritesheet with Mario and Yoshi, SMW-style |
| `usr01/exp/Mail/inbox/1` | Mon, 22 Mar 93 19:10:31 JST | test | (none) | `test` | Hironobu Kakui's terminal rice, lol (`.cshrc`) |
| `usr01/noa/Mail/inbox/1` | Thu, 11 Mar 93 22:55:09 JST | approval ver 0.1 | `angry` | `PRG.ROM` | Prototype program code for Kirby's Adventure (NES) |
| `usr01/noa/Mail/inbox/#2` | Fri, 19 Mar 93 13:30:14 JST | DMG: JIMMY CONNORS TENNIS | `antepaenultima` | `DMGJCX00.PRG` | ROM of Jimmy Connors Tennis (Game Boy), fully playable |
| `usr01/rd1/Mail/inbox/1` | Wed, 3 Mar 93 10:01:48 JST | minesweeper game | `izushi` | `WINMINE.EXE`<br>`WINMINE.INI` | [Windows 3.1 Minesweeper](https://en.wikipedia.org/wiki/Microsoft_Minesweeper) + a config file with the name `nishizawa` |
| `usr01/uji/Mail/inbox/#1` | Wed, 17 Mar 93 08:37:50 JST | bubsy and waldo | `pmdawn` | `SGW05.COM`<br>`SUY02-0.COM`<br>`SUY02-1.COM`<br>`SUY02-2.COM`<br>`SUY02-3.COM` | ROMs for Bubsy in: Claws Encounters of the Furred Kind and The Great Waldo Search (both SNES) |
| `usr01/uji/Mail/inbox/1` | Thu, 18 Mar 93 08:28:41 JST | dungeons and dogs | `sickboy` | `SWW07-0.COM`<br>`SWW07-1.COM` | ROM of Wayne's World (SNES, retail) |
| `usr01/uji/Mail/inbox/2` | Fri, 19 Mar 93 09:11:09 JST | SFRG-FO[Ver.00] | `starwing` | `SFRG-FO.rom` | German ROM of Starwing (EU Star Fox), SNES |
| `usr01/uji/Mail/inbox/3` | Sat, 20 Mar 93 08:36:29 JST | 5 games | `toomanygames` | `NHI02.CHR`<br>`NHI02.COM`<br>`NU803-0.COM`<br>`NU803-1.COM`<br>`SHX01-0.COM`<br>`SHX01-1.COM`<br>`SMU00-0.COM`<br>`SMU00-1.COM`<br>`STZ02-0.COM`<br>`STZ02-1.COM` | ROMs of: <ul><li>Hit the Ice (NES)</li><li>Battletoads/Double Dragon (NES), retail</li><li>Super High Impact (SNES)</li><li>Mario is Missing (SNES)</li><li>Taz-Mania (SNES)</li></ul>


## Other files
- `/etc/passwd`: File containing various DES-hashed passwords; fairly easy to crack. Most are simple Japanese words (root password is `famicom`, etc)
- `/local/*`: Contains various system binaries and scripts, some custom for mail encryption. All either shell scripts or MIPS32BE binaries.
- `/usr02/hallab/.tmp/vs0301.lzh`: Contains a Vegas Stakes SNES ROM, potentially prototype build? File timestamps are `1993-03-01`. Boots fine in an emulator, SHA256s below:
```
7e5f0898e0c27f54c813b54450d36dd095321edf6aa0699321c78a0a10574421 VS1.COM
5c2478d27cee52067a55130f6f624aa66ff00872adf80896d828bc0062668429 VS2.COM
```

And concatenated: `799974a8488a315a10e303a406e6587029a2e7f98ffd1b8ad3127d5780989a02`

## Found ROMs
A number of the files contain various NES, SNES and Game Boy ROMs. The status of those can be seen below:

| Filename | Game | Console | File date | CRC32 | Status | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| `DMGJCX00.PRG` | Jimmy Connors Tennis | Game Boy | 1993-03-19 | 9B7EBF91 | [Retail](https://datomatic.no-intro.org/index.php?page=show_record&s=46&n=0596) |
| `NHI02.COM` / `NHI02.CHR` | Hit the Ice | NES | 1993-03-11 | DB0F299A | [Retail](https://twitter.com/frankcifaldi/status/1287443401416142848) | PRG and CHR separate. This game was never actually released, so despite being "retail" (passed approval and lot-check), this build was previously unknown. |
| `NU803-#.COM` | Battletoads/Double Dragon | NES | 1993-03-08 | CEB65B06 | [Retail](https://datomatic.no-intro.org/?page=show_record&s=45&n=0226) |
| `PRG.ROM` | Kirby's Adventure | NES | 1993-03-11 | n/a | Prototype? (partial) | PRG ROM only. [Dated the day before official, but still differs from retail PRG.](https://discordapp.com/channels/736679118258700289/737039774191452251/737123723810701366) May be an RC? |
| `SFRG-FO.rom` | Star Wing | SNES | 1993-03-18 | 42275E73 | Unknown | German localization |
| `SGW05.COM` | The Great Waldo Search | SNES | 1993-03-05 | EB49F246 | [Retail](https://datomatic.no-intro.org/?page=show_record&s=49&n=0992) |
| `SHX01-#.COM` | Super High Impact | SNES | 1993-03-11 | B0B76A2D | Unknown |
| `SMU00-#.COM` | Mario Is Missing! | SNES | 1993-03-10 | C2448DDA | Unknown |
| `STZ02-#.COM` | Taz-Mania | SNES | 1993-03-17 | 1AA76D59 | Unknown |
| `SUY02-#.COM` | Bubsy in: Claws Encounters of the Furred Kind | SNES | 1993-03-05 | 444A52C1 | [Retail](https://datomatic.no-intro.org/?page=show_record&s=49&n=0356)
| `SWW07-#.COM` | Wayne's World | SNES | 1993-02-17 | 0D426A15 | [Retail](https://datomatic.no-intro.org/?page=show_record&s=49&n=3250) |
| `VS#.COM` | Vegas Stakes | SNES | 1993-03-01 | 09F6F778 | Unknown |

## File hashes
Post-extraction and un-tarring. SHA256:

```
76229c70c397f640be3cd3070802bc202831005a3a1bf575de5a0326b6caa670 NEWS_02/usr01/eng/Mail/inbox/1/sromchk/SROMCHK.EXE
103d9dcca68d78c1501615a4807bc39492fadc45447e815680689a7401f07a79 NEWS_02/usr01/eng/Mail/inbox/2/send0312/SGE.ENV
cb1e20f323d5d25f639d860c5477d475dbd3d029fb78613891ad4f2365b5198b NEWS_02/usr01/eng/Mail/inbox/2/send0312/SGE.EXE
bcf8d59012d08073d2c2d330870c34d75c207888fa5352d03b3f3a9bd0474b21 NEWS_02/usr01/eng/Mail/inbox/2/send0312/SGE.OVR
fce4ea96a4b8faab17e1c275aac37798f9759fd5e61dc27ca5e98a380e540fa3 NEWS_02/usr01/eng/Mail/inbox/2/send0312/SME.EXE
b7b2cceb3ca27acc8128d406679838db2adef7af303ec378508341ceda14ae92 NEWS_02/usr01/eng/Mail/inbox/2/send0312/SME.OVR
20b359f84c5404e0a7e663811680d76e3d53871dd2d8ee907f219fa032e205ff NEWS_02/usr01/eng/Mail/inbox/2/send0312/SWM.EXE
8a0a4047921b2f17f1d93e13b92343697e51688d1f603a9aa503971b71129096 NEWS_02/usr01/eng/Mail/inbox/2/send0312/SWM.OVR
0f7cd93aef7b5708c235b741cf133880008aa3ec101d8f8a116b036996ae8144 NEWS_02/usr01/eng/Mail/inbox/3/isw/ISW.COM
3146fc7dcd8ae24d2432094909666620ca572a33a73e8e9c1793f1506f63d01f NEWS_02/usr01/eng/Mail/inbox/3/isw/ISW.EXE
3a2c0360c3210033144ea3f35937ec833125f0fe9d9db06e53543fd3bdcd3c51 NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISASMN/IS65.EXE
9ab522f9124e388bed1f665c352b44b5161bf14a9ab7d261c428c749cf4d3f8c NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISASMN/ISLINK.EXE
77711daadfbf5ab78045ffb1d2c808f1ea358b373a2dddfa68cf94bade4842cc NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISASMN/ISSND.EXE
e9e889e4ad92121c042a96d17e0a8e3f4ca5412add1f7662916262fda3b40d11 NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/ISW.COM
29e52e328e2f5b43e26204ce8b0b6e19a6337af1d3c14460ec11fe5e677aa4eb NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/ISW.EXE
2a79e29b73391d8a7c7bcd2e7591aa5e4fb73523e1b5f83dbf3f73b16f8ee592 NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/ISWASM.BAT
4cb4084e7ece3b7aedca7d9caaff60e64a90bc878a6b321626678bf3e73758e9 NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/ISWEDIT.BAT
56b73bcabeea4e09b28f789c61bd31838cd22d461aa26a91fe6c8344ed4f4946 NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/ISWREQ.COM
dc64b81ddafb6c2998f23786fa4e6d281374ae452df5066ef2adcab7d2d9b99c NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/TEST1.X65
3bbaa001201d981b942d9592f07817e35a0e27cf75098579da972a3c3927213c NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/TEST2.X65
4a97ae46137e3df00065a84f2e6ee6090a88df8a4d1c2f047bae7922eb2264fd NEWS_02/usr01/eng/Mail/inbox/4/iswn0318/ISW0318/TEST3.X65
6a2b488eeb157e3b229e2be1d3ae3d4ead5ca8589d5491a1ddf51597964243ca NEWS_02/usr01/eng/Mail/inbox/5/mario-4/CHIJO.COL
67b307974c36e50e2cd6818759dc340e29e03646b9e321e6d0f9ef7048931725 NEWS_02/usr01/eng/Mail/inbox/5/mario-4/M-POSE.CGX
c5632ab70cdcc0a730ee48ec226cac9bf04251f64f74808549a9fb42459918bf NEWS_02/usr01/eng/Mail/inbox/5/mario-4/M-POSE.OBJ
24e3f21b19f2ee450ba7d75175a811cfdf1cd018b19dda7e7fa0713eefb1c431 NEWS_02/usr01/eng/Mail/inbox/5/mario-4/RUN.OBJ
fefa2413f1b354223c493ba32929f272c46472cf244f5c41d64365eeedb23959 NEWS_02/usr01/eng/Mail/inbox/5/mario-4/YOSHI.CGX
fc06b1d5f7bbb84f7eddb57f13eb92549ff9de5123eedec82aea7f01700e6002 NEWS_02/usr01/eng/Mail/inbox/5/mario-4/YOSHI.OBJ
1753fa2e57693c2ae12fc12b9a61b4ffaa61046279c0db3380de25acd18466a0 NEWS_02/usr01/exp/Mail/inbox/1/test
0b1f15c6ab2bbdf1e2c4138a18be8bf480d0b2144589cfebd2058415fadc2d77 NEWS_02/usr01/noa/Mail/inbox/#2/mar19th/DMGJCX00.PRG
a1274dbd08bed8052acbe89f25d027d7e21df02a192a51f48b27d9ed92689737 NEWS_02/usr01/noa/Mail/inbox/1/neskr/PRG.ROM
633c1328922b4092351e0019b1ba1c6d56c0f0fea83824cc8b7e2e731b15e80b NEWS_02/usr01/rd1/Mail/inbox/1/MINES/WINMINE.EXE
60771697744c11f2b71c9938f5a0c144b3869299b675839244a4edf573fe2c51 NEWS_02/usr01/rd1/Mail/inbox/1/MINES/WINMINE.INI
3ab1ca181000f10cb7c2ae8dc9fafeecd5d77314ee92960e26dff0d6a1fd11ee NEWS_02/usr01/uji/Mail/inbox/#1/mar16/SGW05.COM
71cbf2cfa4723f2b2797cc6c77c6beacce2c4eca8efdeec7358d11df5bfd6ce2 NEWS_02/usr01/uji/Mail/inbox/#1/mar16/SUY02-0.COM
b59171e14642fec45abd08382e540d54105bde368b2e691d23d9e49f1486fa27 NEWS_02/usr01/uji/Mail/inbox/#1/mar16/SUY02-1.COM
3a9ebc4f96980c49d33eae8394c36e5a1e251a0919c30c60c47c80317e321802 NEWS_02/usr01/uji/Mail/inbox/#1/mar16/SUY02-2.COM
436be35456cb0001fbc766974510ca0834cedde2b776f3514b26898ed511882b NEWS_02/usr01/uji/Mail/inbox/#1/mar16/SUY02-3.COM
edeb34d5e55aa447c6bab0008c98473f3348207a9595ce8839b26e2e7cfa13cd NEWS_02/usr01/uji/Mail/inbox/1/feb17/SWW07-0.COM
a5365d940e71c2188f5f9754f11bb235296b407cdfc8c27c5bbfc3dcf3f79e68 NEWS_02/usr01/uji/Mail/inbox/1/feb17/SWW07-1.COM
960131d8a4aa3fd00d3d2822c7480783dc7056c48bc315aa1926456767e7defc NEWS_02/usr01/uji/Mail/inbox/2/SFRG-FO/SFRG-FO.rom
7252f1cb44f0beed1ee331e194fb637635006fada138f47af5814e0ff37f3032 NEWS_02/usr01/uji/Mail/inbox/3/mar19/NHI02.CHR
54019541e3fbdf0e1fa0c1158b3de8b62251f1a374bf454d65b617c36fabba61 NEWS_02/usr01/uji/Mail/inbox/3/mar19/NHI02.COM
097c564150796f81c4061d87baa38631ba91e9b8302e471e8f90e7a86feb07f5 NEWS_02/usr01/uji/Mail/inbox/3/mar19/NU803-0.COM
fc992a7591f4ffa3bdabccc8b7348a86423dbb812e67aaf01b193ee2ef21f1b1 NEWS_02/usr01/uji/Mail/inbox/3/mar19/NU803-1.COM
465bd4f0378000f80fb0bc6ca31bf6e21ab3a3ddbc1da50f8b0720f55cd79455 NEWS_02/usr01/uji/Mail/inbox/3/mar19/SHX01-0.COM
7b923ffd9925e6c04ebc123fd56624e88419806989f38cabce32f2f68b74da76 NEWS_02/usr01/uji/Mail/inbox/3/mar19/SHX01-1.COM
ff332cb3fd50e32db6aa0ddaa413f380407b853b3c9a9eb41694cbf9be3ef712 NEWS_02/usr01/uji/Mail/inbox/3/mar19/SMU00-0.COM
733c9f75b3c6d66a2b7de35550b228492165ff36509e970a2078657ff6904c68 NEWS_02/usr01/uji/Mail/inbox/3/mar19/SMU00-1.COM
20a110d485f34ef559326913aa8d0e567845079d9141892c546116b5bbee85c5 NEWS_02/usr01/uji/Mail/inbox/3/mar19/STZ02-0.COM
006d066e42ee8de615b8f46b429960cf2cfcc88960cad61e903a47effc9afbf4 NEWS_02/usr01/uji/Mail/inbox/3/mar19/STZ02-1.COM
```
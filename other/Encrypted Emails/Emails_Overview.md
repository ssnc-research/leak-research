# The Emails
Found among the NeWs workstation files are a collection of encrypted emails between Nintendo of Japan and Nintendo of America. No actual text can be pulled from these emails, but the file attachments are available after being decoded from ASCII85.

All information below is organized in a Google spreadsheet as well, for those who prefer that: [Overview of Email Files](https://docs.google.com/spreadsheets/d/1vLM87ecRqLLgoc8xCCSfy0ZhDe7Z9dyvN2uGcB-smzk/edit?usp=sharing)

---

### From: Hironobu Kakui / To: NOA
#### Sent 1993-03-11

- **Kirby's Adventure (NES)**
`7EFAA47E5F61394741EC78239A59A2C7C0047099`
Only the PRG.ROM is present, the CHR.ROM is missing. Doesn't match hashes of any known PRG.ROM in retail builds. Replacing the PRG in a NTSC ROM with this shows no immediate differences.

---

### From: NOA / To: Uji
#### Sent 1993-03-20

- **Jimmy Connor's Tennis (Game Boy)**
`06C9018F7943362F8B9571486B6412DE11672718`
Previously undumped build of the game, doesn't match hashes of other known builds.

---

### From: NOA / To: Uji
#### Sent 1993-03-17

- **The Great Waldo Search (SNES)**
`C5B82F708306B3186005FB1803494F8185D1DA40`
Matches the USA retail version, present on No-Intro.

- **Bubsy in Claws Encounters of the Furred Kind (SNES)**
`9F6FF6264E0361E074F9CFEE2EC4976866A781C5`
Matches the USA retail version, present on No-Intro.

---

### From: NOA / To: Uji
#### Sent 1993-03-18

- **Wayne's World (SNES)**
`0681D155D6BA4C315AFBB52FBB3B7C053A0935FF`
Matches USA retail version, present on No-Intro.

---

### From: Hironobu Kakui / To: Uji
#### Sent 1993-03-19

- **Starwing (Star Fox) (SNES)**
`90C2BF8BAE58BCD5AD104EF0AAB44A2B21975093`
Hash matches no known build. The game is in German-only, possible localization prototype.

---

### From: NOA / To: Uji
#### Sent 1993-03-20

- **Hit the Ice (NES)**
`CE963A59444D8AE5D56E0E95FE63D568CE244E0C`
Matches no known builds. The approved final build of this game, more complete than the prototype dumped in 2002 by Frank Cifaldi.

- **Battletoads Double Dragon (NES)**
`A14563325B0F33C358142E7363D31614722FDDB1`
Headerless ROM, but once fixed it matches No-Intro dump.

- **Super High Impact (SNES)**
`FA9A48B0F1DF82331A5DA173FA501863B5105E53`
Previously undumped build of the game, doesn't match hashes of other known builds. No immediate differences from retail.

- **Mario is Missing (SNES)**
`0936084DC3BF1E2EAF90277E54D0D76A786C187F`
Previously undumped build of the game, doesn't match hashes of other known builds. No immediate differences from retail.

- **Taz-Mania (SNES)**
`200E92DCE73B3CE1AD74A7826E2D0A9DB21A0939`
Previously undumped build of the game, doesn't match hashes of other known builds. No immediate differences from retail.

---

### From: NOA / To: R&D1
#### Sent 1993-03-03

- **Windows Minesweeper v3.10.0.103**
Windows executable file of Minesweeper, runs in DOSBOX.

- **WINMINE.INI**
Settings file for Minesweeper.

---

### From: Katoh / To: Kenji Nishizawa
#### Sent 1993-02-10

- **SNES Rom Check Tool**
MS-DOS executable, boots in DOSBOX.

---

### From: Keisuke Terasaki / To: Tsuyoshi Kiyuna
#### Sent 1993-03-12

- **SNES Gakufu Editor v1.10b**
MS-DOS executable, boots in DOSBOX.

- **SNES Music Editor v1.10b**
MS-DOS executable, boots in DOSBOX.

- **SNES Sound Wave Maker v1.10b**
MS-DOS executable, boots in DOSBOX.

---

### From: R&D1 / To: NOA
#### Sent 1993-03-15

- **SNES DevKit Debugger**
MS-DOS executable, boots in DOSBOX but crashes often.

---

### From: Keisuke Terasaki / To: NOA
#### Sent 1993-03-18

- **Unknown Development Tool**
Four MS-DOS executables seem to work together to form a type of development tool, but none will boot in DOSBOX.

- **SNES DevKit Debugger**
MS-DOS executable, boots in DOSBOX but crashes often.

- **Two Batch Files**
One batch files attempts to run the three snippets of 6502 assembly code present in the folder, while the other calls for something called "vzibm".

---

### From: Hironobu Kakui / To: NOA
#### Sent 1993-03-19

- **Super Mario World - Color Palette**
Displays incorrectly in SRDCAD, potenital bad header data.

- **Super Mario World - Sprite File**
[M-POSE.CGX](https://drive.google.com/file/d/1DBlvNth14Lf5YuHlrb-YZfe0ioyBLsNL/view?usp=sharing)

- **Super Mario World - Sprite File**
[YOSHI.CGX](https://drive.google.com/file/d/1OjjaG7GQgYG4oK8ymjbYHoDBcoMSehwZ/view?usp=sharing)

- **Super Mario World - Animation**
[M-POSE.OBJ](https://drive.google.com/file/d/1gDcptk9tBMAGAhLm_tNc1I3U7OxX3o-t/view?usp=sharing)

- **Super Mario World - Animation**
[RUN.OBJ](https://drive.google.com/file/d/1Ne67msB4SP9dlUxBtv_FrBhiuxMu1t40/view?usp=sharing)

- **Super Mario World - Animation**
[YOSHI.OBJ](https://drive.google.com/file/d/1u6bgWxKp2ock4LvY-5qj-_2qDrlCspsb/view?usp=sharing)
import sys, os, io

from cgx2png import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PIL import Image

class DragDropLineEdit(QLineEdit):
    def __init__(self, *args):
        super().__init__(*args)

    def dragEnterEvent(self, evt):
        data = evt.mimeData() 
        urls = data.urls()
        if urls and urls[0].scheme() == "file":
            evt.acceptProposedAction()

    def dragMoveEvent(self, evt):
        data = evt.mimeData() 
        urls = data.urls()
        if urls and urls[0].scheme() == "file":
            evt.acceptProposedAction()

    def dropEvent(self, evt):
        data = evt.mimeData() 
        urls = data.urls()
        if urls and urls[0].scheme() == "file":
            path = urls[0].toLocalFile()
            self.setText(path)

class BackgroundTab(QWidget):
    def __init__(self, cgx, *args):
        super().__init__(*args)
        self.cgx = cgx

        self.preview = ScrollImage()

        self.tilemap_box = QSpinBox()
        self.tilemap_box.setMaximum(4096-1024)
        self.tilemap_box.setSingleStep(1024)
        self.tilemap_box.valueChanged.connect(self.render)

        self.size_box = QComboBox()
        self.size_box.addItem("32x32", 0)
        self.size_box.addItem("64x64", 1)
        self.size_box.currentIndexChanged.connect(self.render)

        layout = QFormLayout()
        layout.addRow("Tilemap offset", self.tilemap_box)
        layout.addRow("Size", self.size_box)

        vbox = QVBoxLayout()
        vbox.addLayout(layout)
        vbox.addWidget(self.preview)

        self.setLayout(vbox)

    def render(self):
        if not (self.cgx.tilemap and self.cgx.palette and self.cgx.tileset):
            return False

        depth = int(self.cgx.bit_depth.currentData())
        if self.size_box.currentData() == 1:
            self.image = create_bg_image_big(self.cgx.tilemap, self.cgx.tileset, self.cgx.palette, depth, False, self.tilemap_box.value())
        else:
            self.image = create_bg_image(self.cgx.tilemap, self.cgx.tileset, self.cgx.palette, depth, False, self.tilemap_box.value())
        self.preview.set_image(self.image, self.cgx.scale.currentData())
        return True

class TilesetTab(QWidget):
    def __init__(self, cgx, *args):
        super().__init__(*args)
        self.cgx = cgx

        self.preview = ScrollImage()

        self.width_box = QSpinBox()
        self.width_box.setMinimum(1)
        self.width_box.setValue(16)
        self.width_box.valueChanged.connect(self.render)
        
        self.palette_box = QSpinBox()
        self.palette_box.setMaximum(256-16)
        self.palette_box.setSingleStep(16)
        self.palette_box.valueChanged.connect(self.render)

        layout = QFormLayout()
        layout.addRow("Width", self.width_box)
        layout.addRow("Palette Offset", self.palette_box)

        vbox = QVBoxLayout()
        vbox.addLayout(layout)
        vbox.addWidget(self.preview)

        self.setLayout(vbox)

    def render(self):
        if not (self.cgx.palette and self.cgx.tileset):
            return False

        self.image = create_tileset_image(self.cgx.tileset, self.cgx.palette, self.width_box.value(), self.palette_box.value())
        self.preview.set_image(self.image, self.cgx.scale.currentData())
        return True

class ScrollImage(QScrollArea):
    def __init__(self, *args):
        super().__init__(*args)

        self.label = QLabel()
        
        self.setWidgetResizable(True)
        self.setAlignment(Qt.AlignCenter)
        self.setWidget(self.label)

    def set_image(self, img, scale):
        self.image = img

        bio = io.BytesIO()
        img.save(bio, format="BMP")

        qp = QPixmap(img.width, img.height)
        qp.loadFromData(bio.getvalue())
        qp = qp.scaled(img.width * scale, img.height * scale)

        self.label.setPixmap(qp)
        self.label.setFixedSize(qp.width(), qp.height())

class Cgx(QMainWindow):
    def __init__(self, *args):
        super().__init__(*args)
        
        self.tilemap = None
        self.tileset = None
        self.palette = None
        self.object = None

        self.bg_page = BackgroundTab(self)
        self.ts_page = TilesetTab(self)

        self.tabs = QTabWidget(self)
        self.tabs.addTab(self.bg_page, "Background")
        self.tabs.addTab(self.ts_page, "Tileset")
        
        self.cgx_pick = DragDropLineEdit()
        self.cgx_pick.textChanged.connect(self.reload_cgx)

        self.col_pick = DragDropLineEdit()
        self.col_pick.textChanged.connect(self.reload_col)

        self.scr_pick = DragDropLineEdit()
        self.scr_pick.textChanged.connect(self.reload_scr)

        def bit_depth_changed():
            self.reload_cgx(self.cgx_pick.text())

        self.bit_depth = QComboBox()
        self.bit_depth.addItem("2 bpp (4-color)", 2)
        self.bit_depth.addItem("4 bpp (16-color)", 4)
        self.bit_depth.addItem("8 bpp (256-color)", 8)
        self.bit_depth.currentIndexChanged.connect(bit_depth_changed)
        self.bit_depth.setCurrentIndex(1) # 4bpp default

        self.scale = QComboBox()
        self.scale.addItem("100%", 1)
        self.scale.addItem("200%", 2)
        self.scale.addItem("300%", 3)
        self.scale.addItem("400%", 4)
        self.scale.addItem("500%", 5)
        self.scale.currentIndexChanged.connect(self.render)

        # lol @ all of this
        def add_file_browse(edit):
            layout = QHBoxLayout()
            layout.addWidget(edit, stretch=1)

            button = QPushButton("...")
            button.setMaximumWidth(25)
            layout.addWidget(button)

            def handler():
                path, _ = QFileDialog.getOpenFileName(self)
                edit.setText(path)

            button.clicked.connect(handler)
            return layout

        self.refresh_btn = QPushButton("Refresh")
        self.refresh_btn.clicked.connect(self.render)

        self.save_btn = QPushButton("Save")
        self.save_btn.setEnabled(False)
        self.save_btn.clicked.connect(self.save)

        self.preview = QLabel()

        btns = QHBoxLayout()
        btns.addWidget(self.refresh_btn, stretch=1)
        btns.addWidget(self.save_btn, stretch=1)
    
        layout = QFormLayout()
        layout.addRow(".cgx file", add_file_browse(self.cgx_pick))
        layout.addRow(".col file", add_file_browse(self.col_pick))
        layout.addRow(".scr file", add_file_browse(self.scr_pick))
        layout.addRow("Bit depth", self.bit_depth)
        layout.addRow("Scale", self.scale)
        layout.addRow(btns)

        hbox = QHBoxLayout()
        hbox.addLayout(layout)
        hbox.addWidget(self.tabs, stretch=1)

        self.central = QWidget()
        self.central.setLayout(hbox)
        self.setCentralWidget(self.central)

    def reload_cgx(self, path):
        if not os.path.isfile(path):
            return

        data = strip_scad_footer(open(path, "rb").read())

        try:
            depth = int(self.bit_depth.currentData())
            self.tileset = decode_tileset(data, depth)
            self.render()
        except Exception as e:
            print(e, file=sys.stderr)

    def reload_col(self, path):
        if not os.path.isfile(path):
            return

        data = strip_scad_footer(open(path, "rb").read())

        try:
            self.palette = decode_palette(data)
            self.render()
        except Exception as e:
            print(e, file=sys.stderr)

    def reload_scr(self, path):
        if not os.path.isfile(path):
            return

        data = strip_scad_footer(open(path, "rb").read())

        try:
            self.tilemap = decode_tilemap(data)
            self.render()
        except Exception as e:
            print(e, file=sys.stderr)


    def render(self):
        tab = self.tabs.currentWidget()
        scale = self.scale.currentData()

        bg_success = self.bg_page.render()
        ts_success = self.ts_page.render()
        if not (bg_success or ts_success):
            return

        self.save_btn.setEnabled(True)

    def save(self):
        tab = self.tabs.currentWidget()
        if not tab.image:
            return
            
        filename, _ = QFileDialog.getSaveFileName(self, filter="PNG file (*.png)")
        if not filename:
            return

        scale = self.scale.currentData()
        scaled = tab.image.resize((tab.image.width * scale, tab.image.height * scale), resample=Image.NEAREST)
        scaled.save(filename, format="PNG")
        

if __name__ == "__main__":
    app = QApplication(sys.argv)

    win = Cgx()
    win.show()

    sys.exit(app.exec_())
# cgx2png

A small toolset for viewing and converting files in the `cgx`, `col` and `scr` file format.

Also includes a light GUI, which supports loading tilesets (cgx+col) and tilemaps (cgx+col+scr), 2/4/8-bit tiles, live(-ish) previews, and exporting to PNG at various scales.

![Screenshot of GUI](screenshot.png)

## Usage

### CLI

```sh
# For converting a full tilemap
python3 cgx2png.py -t tileset.cgx -p palette.col -m tilemap.scr -d <2|4|8> -o outfile.png

# For converting a tileset (no tilemap)
python3 cgx2png.py -t tileset.cgx -p palette.col -d <2|4|8> -w <width> -o outfile.png
```

Requires Pillow, so: `pip3 install --upgrade Pillow` first.

If on Windows, you may need to type `python` instead of `python3`. Especially do this if it seems like the program just exits immediately without outputting anything.

### GUI

Run `gui.py` in the same folder as `cgx2png.py`. Double-clicking may be enough.

On top of Pillow (see above), also requires PySide2, so: `pip3 install --upgrade PySide2`.

## Format description

**CGX** contains tilesets of 8x8 tiles (often in logical rows of 16 tiles). This usually means tiles for tilemaps, sprites, animation sheets, etc.
CGX files do *not* contain any color or "position" information by themselves, but each pixel is merely an index into a separate color palette.
Tiles can be in either 2-bit, 4-bit or 8-bit mode, giving a total of 4, 16, and 256 colors, respectively.

The file format maps directly onto the SNES's tile VRAM layout, in a strange interleaved planar layout. For more information, see:

- https://www.princed.org/wiki/SNES_format#Tiles
- https://wiki.superfamicom.org/backgrounds#toc-2
- https://en.wikibooks.org/wiki/Super_NES_Programming/Graphics_tutorial#Tile_format

**COL** files contain color palettes of 15-bit color (5 bits per channel), and up to 256 colors per file.

They're simply a list of 16-bit integers (little-endian) with the following bit format:

   xBBBBBGGGGGRRRRR

storing 5 bits of the red, green, and blue channel (multiply by 8 to get the standard 0-255 range).

**SCR** files contain a full SNES tile map, usually (part of) a level, a title screen, overlay graphics, etc.
These are a 32x32 grid of tile numbers (referring to the position in the corresponding CGX file), along with potentially flipping each tile.

At lower bit depths (2-bit/4-bit), they can also contain a per-tile palette offset, so 4-bit tilesets can make use of a full palette. If you're trying to dump a CGX/COL pair without a SCR and seeing blank output, this is likely what's happening!

Like the CGX files, these directly map to the SNES's tilemap memory. For more info, see:

- https://www.princed.org/wiki/SNES_format#Maps
- https://wiki.superfamicom.org/backgrounds#toc-2

Note that a lot of the graphics files in the leaks don't "neatly" match up. Sometimes you have to hunting around for the proper pairings of CGX, COL, and SCR files to get the correct (or just a decent) output.
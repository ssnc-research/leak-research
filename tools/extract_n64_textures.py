"""Finds textures that have been converted to N64 source code and restores them
as PNGs.
"""

# Extracts N64 textures that have been converted to N64 source code and saves
# them as PNGs inside of a "textures" directory. Requires pypng to be
# installed. To use, run inside of a supported directory. The directory must
# already be CVS checked out, i.e. no v files.

# Sample Windows usage:
# Install Python 3 from https://www.python.org/downloads
# Win + R to bring up "Run" dialog
# cmd
# pip3 install pypng
# cd D:\wherever\sm64
# python3 C:\Downloads\extract_n64_textures.py

# Copyright 2020 Brandon Skari
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import array
import collections
import codecs
import enum
import glob
import hashlib
import io
import itertools
import os
import pathlib
import re
import sys
import typing

try:
    import png
except:
    print("You need to install pypng")
    sys.exit(1)


OUTPUT_DIRECTORY = "textures"
UNKNOWN_FILE_NAME_COUNT = 0
ALREADY_SAVED_MD5S = set()


def convert_file(file_name: str) -> None:
    """Converts a single file."""
    current_directory = str(pathlib.Path().absolute())
    called = False
    for game, function in GAME_DIRECTORY_TO_CONVERTER.items():
        if game in current_directory:
            try:
                os.mkdir(OUTPUT_DIRECTORY)
            except:
                pass

            print(f'Converting textures for {game} into directory "{OUTPUT_DIRECTORY}"')
            function((file_name,))
            called = True
            break

    if not called:
        print("File from an unknown directory and can't be converted, sorr.")
        print("Unfortunately, each game has its own way to save textures.")
        print("Conversion code has to be written per game.")
        print("To determine which game was being converted, this script just checks")
        print("directories and looks for a supported game folder.")
        print(f"Right now, the script only supports: {', '.join(GAME_DIRECTORY_TO_CONVERTER.keys())}")


def convert_all_files() -> None:
    """Converts all the files in a particular N64 directory to PNGs."""
    # Different games use different formats, so we need to have different parsers
    current_directory = str(pathlib.Path().absolute())
    called = False

    def chain_globs(targets: typing.List[str]) -> typing.Iterator[str]:
        for wild_card in targets:
            for file_name in glob.iglob(wild_card, recursive=True):
                yield file_name
    game_to_texture_extensions = {
        "dm64": chain_globs(("**/*.txt",)),
        "f0x": chain_globs(("**/*.c", "**/*.tex")),
        "forest": chain_globs(("**/*.mdl", "**/*.c")),
        "sm64": chain_globs(("**/*.h", "**/*.c", "**/*.sou", "**/*.tex", "**/*.txt")),
        "wr64": chain_globs(("**/*.tex", "**/*.c", "**/*.h")),
    }
    assert set(game_to_texture_extensions.keys()) == set(GAME_DIRECTORY_TO_CONVERTER.keys())

    for game, function in GAME_DIRECTORY_TO_CONVERTER.items():
        if game in current_directory:
            try:
                os.mkdir(OUTPUT_DIRECTORY)
            except:
                pass

            print(f'Converting textures for {game} into directory "{OUTPUT_DIRECTORY}"')
            function(game_to_texture_extensions[game])
            called = True
            break

    if not called:
        print("This needs to be run from within a supported N64 directory.")
        print(f"Currently supported directories: {', '.join(GAME_DIRECTORY_TO_CONVERTER.keys())}")


def get_line_iterator(file_name: str) -> typing.Iterator[str]:
    """Returns a line iterator for a file."""
    # Just try all of the Japanese codecs until one works, I'm too lazy to find
    # out which one they used in 1994
    for codec in (
        "utf8",
        "cp932",
        "euc_jp",
        "euc_jis_2004",
        "euc_jisx0213",
        "iso2022_jp",
        "iso2022_jp_1",
        "iso2022_jp_2",
        "iso2022_jp_2004",
        "iso2022_jp_3",
        "iso2022_jp_ext",
        "shift_jis",
        "shift_jis_2004",
        "shift_jisx0213",
    ):
        success = False
        try:
            with codecs.open(file_name, "r", codec) as file:
                for line in file:
                    yield line
        except:
            continue
        else:
            # That codec worked! Let's abort
            return


def convert_sm64(file_names_iterator: typing.Iterator[str]) -> None:
    """Converts all SM64 texture files and saves them into a folder."""
    # It looks like SM64 only has RGBA5551 and IA. I've only seen G_IM_FMT_RGBA
    # and G_IM_FMT_IA anway. Also, it appears all textures have "txt" or "TxT"
    # in the name of the array.
    texture_regex = re.compile(r"unsigned ((char|short)) \w*(txt)(?i)\w*\[\] = {")
    for file_name in file_names_iterator:
        try:
            iterator = get_line_iterator(file_name)
            for line in iterator:
                match = texture_regex.search(line)
                if not match:
                    continue

                file_format = match.groups()[0]
                if file_format == "char":
                    new_iterator = itertools.chain([line], iterator)
                    convert_ia_c_file(new_iterator, 8)
                elif file_format == "short":
                    new_iterator = itertools.chain([line], iterator)
                    convert_rgba5551_c_file(new_iterator)
        except Exception as exc:
            print(f"Failed to parse {file_name}: {exc}")


def convert_f0x(file_names_iterator: typing.Iterator[str]) -> None:
    """Converts all F0X texture files and saves them into a folder."""
    # F0X uses G_IM_FMT_A, G_IM_FMT_CI, G_IM_FMT_I, G_IM_FMT_IA, G_IM_FMT_RGBA
    # It appears all of the textures are converted from some other format to C
    # source files using rgb2c
    for file_name in file_names_iterator:
        iterator = get_line_iterator(file_name)
        convert_rgb2c_c_file(file_name, iterator)


def convert_forest(file_names_iterator: typing.Iterator[str]) -> None:
    """Converts all forest texture files and saves them into a folder."""
    # forest uses CI4, CI8, I4, I8, IA4, IA8, RGBA16, RGBA32
    # The sections look like this:
    # <TEXTURE>
    # {
    #    index 0
    #    name int_tak_tekkin
    #    format CI4
    #    size 16 32  512
    #    byte 0x01 0x02 0x03
    # }
    # There are <PALETTE>, <MATERIAL>, sections too, plus the same sections
    # prepended with EVW, e.g. <EVW_PALETTE>. The material sections have
    # entries for texture and palette, so multiple palettes and materials
    # can be combined. I don't think EVWs are ever explicitly combined with
    # palettes, so I don't know how to deal with ones in CI format, but we can
    # at least save the ones in I and RGBA formats.

    # I wish I was running Python 3.7 and could use dataclasses :(
    class Texture:
        def __init__(
            self,
            name: str,
            format: str,
            size: typing.Tuple[int, int],
            palette: typing.Optional[str],
            data: typing.List[typing.List[int]],
            file_name: str
        ):
            self.name = name
            self.format = format
            self.size = size
            self.palette = palette
            self.data = data
            self.file_name = file_name

            hasher = hashlib.new("md5")
            for row in data:
                hasher.update(array.array("Q", row))
            self.md5_hash = hasher.hexdigest()

        @staticmethod
        def parse(line_iterator: typing.Iterator[str], file_name: str) -> "Texture":
            name = None
            format = None
            size = None
            palette = None
            raw_data = []

            for line in iterator:
                if "}" in line:
                    break
                if "name\t" in line:
                    name = line.split()[1]
                elif "format\t" in line:
                    format = line.split()[1]
                elif "size\t" in line:
                    size = [int(i) for i in (line.split()[1], line.split()[2])]
                elif "palette\t" in line:
                    palette = line.split()[2]
                elif "byte\t" in line or "word\t" in line or "long\t" in line:
                    raw_data += [int(i, 16) for i in line.replace("0x", "").split()[1:]]

            # Format data based on size
            data = []
            width = size[0]
            if format in ("CI4", "IA4"):
                width = width // 2
            row = []
            for index in range(len(raw_data)):
                row.append(raw_data[index])
                if len(row) == width:
                    data.append(row)
                    row = []

            assert name is not None
            assert format is not None
            assert size is not None
            return Texture(name, format, size, palette, data, file_name)

    class Palette:
        def __init__(
            self,
            name: str,
            data: typing.List[int],
        ):
            self.name = name
            self.data = data

        @staticmethod
        def parse(line_iterator: typing.Iterator[str]) -> "Palette":
            name = None
            data = []
            for line in iterator:
                if "}" in line:
                    break
                if "name" in line:
                    name = line.split()[1]
                elif "word" in line or "long" in line:
                    data += [int(i, 16) for i in line.replace("0x", "").split()[1:]]
            assert name is not None
            return Palette(name, data)

    class Material:
        def __init__(
            self,
            name: str,
            texture: str,
            palette: typing.Optional[str],
        ):
            self.name = name
            self.texture = texture
            self.palette = palette

        def __repr__(self):
            return f"name={self.name} texture={self.texture} palette={self.palette}"

        @staticmethod
        def parse(line_iterator: typing.Iterator[str]) -> "Material":
            name = None
            texture = None
            palette = None
            for line in iterator:
                if "}" in line:
                    break
                if "name" in line:
                    name = line.split()[1]
                elif "texture" in line:
                    texture = line.split()[1]
                elif "palette" in line:
                    palette = line.split()[1]
            return Material(name, texture, palette)

    # The values in these dicts are lists, because some names are used multiple
    # times, e.g. the palette `obj_item_peach_pal` and the texture 
    name_to_textures = collections.defaultdict(list)
    name_to_palette = dict()
    # So there are textures and palettes in ef_car01.mdl that are unused, but
    # I'm pretty sure each texture should use the corresponding palette from
    # the same file. So just seed them here.
    materials = [
        Material("FakedCarMaterial1", "obj_s_car_t1_tex", "obj_s_car_pal"),
        Material("FakedCarMaterial2", "obj_s_car_t2_tex", "obj_s_car_pal"),
        Material("FakedCarMaterial3", "obj_s_car_t3_tex", "obj_s_car_pal"),
    ]

    # Let's put the rgb2c files in a different directory so they're easier to
    # look at
    global OUTPUT_DIRECTORY
    default_output_directory = OUTPUT_DIRECTORY
    rgb2c_output_directory = f"{OUTPUT_DIRECTORY}{os.sep}rgb2c"
    try:
        os.mkdir(rgb2c_output_directory)
    except:
        pass

    print("Parsing mdl files")
    for file_name in file_names_iterator:
        iterator = get_line_iterator(file_name)

        if file_name.endswith(".c"):
            OUTPUT_DIRECTORY = rgb2c_output_directory
            convert_rgb2c_c_file(file_name, iterator)
            OUTPUT_DIRECTORY = default_output_directory

        else:
            try:
                for line in iterator:
                    if "<TEXTURE>" in line or "<EVW_TEXTURE>" in line:
                        texture = Texture.parse(iterator, file_name)
                        if (
                            # I don't know why, but some textures have no data, so skip them
                            texture.size[0] != 0
                            and texture.md5_hash not in (t.md5_hash for t in name_to_textures[texture.name])
                        ):
                            name_to_textures[texture.name].append(texture)
                    elif "<PALETTE>" in line or "<EVW_PALETTE>" in line:
                        palette = Palette.parse(iterator)
                        name_to_palette[palette.name] = palette
                    # There doesn't appear to be any EVW_MATERIALS
                    elif "<MATERIAL>" in line:
                        material = Material.parse(iterator)
                        if material.texture is not None:
                            materials.append(material)

            except Exception as exc:
                print(f"Failed to parse {file_name}: {exc}")

    # Save the indexed color textures
    print("Saving paletted textures")
    saved_indexed_texture_md5s = set()
    for material in materials:
        for texture in name_to_textures.get(material.texture):
            png_file_name = f"{texture.file_name.replace(os.sep, '-')}-{texture.name}"
            # The palette might be None if it's an indexed format, so skip
            # those
            if material.palette is not None:
                palette = name_to_palette[material.palette]
                file_format = texture.format
                if file_format == "CI4":
                    save_ci_png(png_file_name, texture.data, palette.data, 4)
                    saved_indexed_texture_md5s.add(texture.md5_hash)
                elif file_format == "CI8":
                    save_ci_png(png_file_name, texture.data, palette.data, 8)
                    saved_indexed_texture_md5s.add(texture.md5_hash)
            else:
                assert texture.format in ("I4", "I8", "IA4", "IA8", "RGBA16", "RGBA32")

    # Some of the EVW_TEXTUREs have a palette also defined, instead of using
    # MATERIAL, so just directly save those
    for textures in name_to_textures.values():
        for texture in textures:
            if texture.palette is not None:
                file_format = texture.format
                if file_format == "CI4":
                    save_ci_png(png_file_name, texture.data, palette.data, 4)
                    saved_indexed_texture_md5s.add(texture.md5_hash)
                elif file_format == "CI8":
                    save_ci_png(png_file_name, texture.data, palette.data, 8)
                    saved_indexed_texture_md5s.add(texture.md5_hash)
                else:
                    assert False, f"{texture.name} has palette but format is {file_format}"

    # Some indexed color textures are never referenced, so save them too
    print("Saving unreferenced paletted textures")
    print(" - This will take a while because we need to guess which palette to use")

    def edit_distance(s1: str, s2: str):
        """Levenshtein distance."""
        if len(s1) > len(s2):
            s1, s2 = s2, s1

        distances = range(len(s1) + 1)
        for i2, c2 in enumerate(s2):
            distances_ = [i2+1]
            for i1, c1 in enumerate(s1):
                if c1 == c2:
                    distances_.append(distances[i1])
                else:
                    distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
            distances = distances_
        return distances[-1]

    # Save the unused textures into a separate directory, so that they're
    # easier to view
    OUTPUT_DIRECTORY = f"textures{os.sep}unused"
    try:
        os.mkdir(OUTPUT_DIRECTORY)
    except:
        pass

    unsaved_indexed_texture_md5s = set()
    for textures in name_to_textures.values():
        unsaved_indexed_texture_md5s.update((
            t.md5_hash for t in textures
            if t.format in ("CI4", "CI8")
        ))
    unsaved_indexed_texture_md5s = unsaved_indexed_texture_md5s - saved_indexed_texture_md5s
    print(f" - Saving {len(unsaved_indexed_texture_md5s)} unreferenced paletted textures into {OUTPUT_DIRECTORY}")
    md5_hash_to_texture = dict()
    for textures in name_to_textures.values():
        for t in textures:
            md5_hash_to_texture[t.md5_hash] = t
    # We should be done with materials now, so drop the ones missing palettes
    materials = [m for m in materials if m.palette is not None]
    for unused_md5_hash in unsaved_indexed_texture_md5s:
        texture = md5_hash_to_texture[unused_md5_hash]
        # Now find a suitable palette
        closest_distance = edit_distance(materials[0].texture, texture.name)
        closest_material = materials[0]
        for material in materials:
            distance = edit_distance(material.texture, texture.name)
            if distance < closest_distance:
                closest_distance = distance
                closest_material = material
                if distance == 1:
                    break

        file_format = texture.format
        png_file_name = f"{texture.file_name.replace(os.sep, '-')}-{texture.name}"
        palette = name_to_palette[closest_material.palette]
        if file_format == "CI4":
            save_ci_png(png_file_name, texture.data, palette.data, 4)
        elif file_format == "CI8":
            save_ci_png(png_file_name, texture.data, palette.data, 8)
        # This is really slow, so give some indication of progress
        sys.stdout.write(".")
        sys.stdout.flush()

    print()
    OUTPUT_DIRECTORY = "textures"

    # Save the direct color textures
    print("Saving direct color textures")
    for textures in name_to_textures.values():
        for texture in textures:
            png_file_name = f"{texture.file_name.replace(os.sep, '-')}-{texture.name}"
            file_format = texture.format
            if file_format == "I4":
                save_i_png(png_file_name, texture.data, 4)
            elif file_format == "I8":
                save_i_png(png_file_name, texture.data, 8)
            elif file_format == "IA4":
                save_ia4_png(png_file_name, texture.data)
            elif file_format == "IA8":
                save_ia8_png(png_file_name, texture.data)
            elif file_format == "RGBA16":
                save_rgba5551_png(png_file_name, texture.data)
            elif file_format == "RGBA32":
                save_rgba32_png(png_file_name, texture.data)


def convert_dm64(file_names_iterator: typing.Iterator[str]) -> None:
    """Converts all dm64 texture files and saves them into a folder."""
    # It looks like image files are stored in .txt files, and they all have a
    # nice header. I only see G_IM_FMT_CI, G_IM_FMT_I, and G_IM_FMT_RGBA.
    # What's weird is that there only a few things rendered as RGBA:
    # count_1_tex, count_2_tex, count_3_tex, ready_a_tex, and ready_b_tex.
    # But each of those also have a lut table defined that's unused?
    # Same thing with G_IM_FMT_I. Maybe we should just find the draw calls,
    # figure out which format they're using, and go from there?

    # First, find all the draw calls
    draw_call_regex = re.compile(r"gsDPLoadMultiBlock\w*\((\w+).*(G_IM_FMT_\w+)")
    type_to_textures = collections.defaultdict(set)

    for file_name in glob.iglob("**/*.nDL", recursive=True):
        for line in get_line_iterator(file_name):
            match = draw_call_regex.search(line)
            if match:
                texture, format = match.groups()
                type_to_textures[format].add(texture)

    # And now start saving the textures
    bit_pre_pix_regex = re.compile(r"BitPrePix=(\d+)")
    texture_regex = re.compile(r"unsigned (char|short) (\w*tex)\[\] = {")
    texture_size_regex = re.compile(r"unsigned short (\w*tex)_size\[\] = { (\d+), (\d+), (\d+)")
    look_up_table_regex = re.compile(r"unsigned (char|short) \w*lut\[\] = {")
    size_regex = re.compile(r"unsigned short \w*_size\[\] = { (\d+), (\d+), (\d+),")

    for file_name in file_names_iterator:
        try:
            bits_per_texel = None
            texture_name = None
            texture_data = None
            look_up_table_data = None
            width = None
            height = None

            iterator = get_line_iterator(file_name)
            for line in iterator:

                match = bit_pre_pix_regex.search(line)
                if match:
                    bits_per_texel = int(match.groups()[0])

                match = texture_regex.search(line)
                if match:
                    texture_name = match.groups()[1]
                    if texture_name is None:
                        return
                    _, texture_data = _get_unsigned_array_data(
                        itertools.chain([line], iterator),
                        texture_regex
                    )

                match = look_up_table_regex.search(line)
                if match:
                    _, lists_look_up_table_data = _get_unsigned_array_data(
                        itertools.chain([line], iterator),
                        look_up_table_regex
                    )
                    # The LUT table needs to be a single list
                    look_up_table_data = []
                    for data_list in lists_look_up_table_data:
                        look_up_table_data += data_list

                match = texture_size_regex.search(line)
                if match:
                    # TODO: Check that the size name matches the texture
                    _, width, height, bits_per_texel = match.groups()
                    width = int(width)
                    height = int(height)
                    bits_per_texel = int(bits_per_texel)

                match = size_regex.search(line)
                if match:
                    width, height, bits_per_texel = [int(i) for i in match.groups()]

            if bits_per_texel is None:
                #print(f"skipping {file_name}")
                continue

            # Some of the converted files will have width and height defined,
            # and we need to format the data accordingly
            if width is not None:
                if bits_per_texel == 4:
                    width /= 2
                rows = []
                row = []
                # Ugh this is slow and ugly
                for value in itertools.chain(*texture_data):
                    row.append(value)
                    if len(row) == width:
                        rows.append(row)
                        row = []

                texture_data = rows
                assert len(texture_data[0]) == width

            if texture_data is None:
                continue
            # Some of the textures appear to be upside-down, so invert them
            if width is None:
                texture_data = list(reversed(texture_data))

            # Now we need to find which file format it is
            if texture_name is not None:
                found_count = 0
                for type, texture_names in type_to_textures.items():
                    if texture_name in texture_names:
                        found_count += 1
                        if type == "G_IM_FMT_I":
                            if bits_per_texel == 4:
                                save_ia4_png(texture_name, texture_data)
                            elif bits_per_texel == 8:
                                save_ia8_png(texture_name, texture_data)
                            else:
                                raise NotImplementedError(
                                    f"In convert_dm64, with file {file_name}, unknown bits_per_texel {bits_per_texel}"
                                )
                        elif type == "G_IM_FMT_CI":
                            save_ci_png(texture_name, texture_data, look_up_table_data, bits_per_texel)
                        elif type == "G_IM_FMT_RGBA":
                            save_rgba5551_png(texture_name, texture_data)

                if found_count != 1:
                    # TODO: But there are some textures that don't appear to be
                    # rendered at all, so just guess
                    if look_up_table_data is None:
                        # Must be an RGB
                        if bits_per_texel == 16:
                            save_rgba5551_png(texture_name, texture_data)
                        else:
                            print(f"{texture_name} wasn't rendered")
                    else:
                        if bits_per_texel is None:
                            if len(look_up_table_data) <= 16:
                                bits_per_texel = 4
                            else:
                                bits_per_texel = 8

                        save_ci_png(texture_name, texture_data, look_up_table_data, bits_per_texel)

        except Exception as exc:
            print(f"Failed to parse {file_name}: {exc}")
            raise


def convert_wr64(file_names_iterator: typing.Iterator[str]) -> None:
    """Converts all WR64 texture files and saves them into a folder."""
    # WR64 has IA8/G_IM_FMT_IA, RGBA16/RGBA32/G_IM_FMT_RGBA, G_IM_FMT_A,
    # G_IM_FMT_CI, G_IM_FMT_I, G_IM_FMT_IA textures. They're stored in .tex
    # files with nice headers. Also some .c and .h.
    image_size_regex = re.compile(r"image size\s*:\s*(\d+) x (\d+)")
    image_size_regex_2 = re.compile(r"Size: (\d+) x (\d+)")
    bit_depth_regex = re.compile(r"Number of bits per texel: (\d+)")
    texture_regex = re.compile(r"unsigned (?:char|short|int) (\w+)\[\] = {")
    format_regex = re.compile(r"format\s*:\s*(\w+)")
    format_regex_2 = re.compile(r"Format of texel: (\w+)")

    for file_name in file_names_iterator:
        try:
            width = None
            height = None
            format = None
            bit_depth = None

            iterator = get_line_iterator(file_name)

            for line in iterator:
                match = image_size_regex.search(line)
                if match:
                    width, height = (int(i) for i in match.groups())
                match = image_size_regex_2.search(line)
                if match:
                    width, height = (int(i) for i in match.groups())
                match = bit_depth_regex.search(line)
                if match:
                    bit_depth = int(match.groups()[0])

                match = texture_regex.search(line)
                if match:
                    if "lut" in line:
                        _, palette_data = _get_unsigned_array_data(
                            itertools.chain([line], iterator),
                            texture_regex,
                        )
                        # We need to flatten palette data into a single array
                        palette_data = itertools.chain(*palette_data)
                    else:
                        texture_name, texture_data = _get_unsigned_array_data(
                            itertools.chain([line], iterator),
                            texture_regex,
                        )

                match = format_regex.search(line)
                if match:
                    format = match.groups()[0]
                match = format_regex_2.search(line)
                if match:
                    format = match.groups()[0]

            if width is not None:
                rows = []
                row = []
                # Ugh this is slow and ugly
                for value in itertools.chain(*texture_data):
                    row.append(value)
                    if len(row) == width:
                        rows.append(row)
                        row = []

                texture_data = rows
                assert len(texture_data[0]) == width

            # So there are some faces in the txtures, but they're upside down.
            # Let's just flip them.
            if "face" in texture_name:
                texture_data = reversed(texture_data)

            if format == "IA8":
                save_ia8_png(texture_name, texture_data)
            elif format == "IA16":
                save_ia16_png(texture_name, texture_data)
            elif format == "RGBA16":
                save_rgba5551_png(texture_name, texture_data)
            elif format == "RGBA32":
                save_rgba32_png(texture_name, texture_data)
            elif format == "CI8":
                save_ci_png(texture_name, texture_data, palette_data, 8)
            elif format == "G_IM_FMT_IA":
                if bit_depth == 4:
                    save_ia4_png(texture_name, texture_data)
                elif bit_depth == 8:
                    save_ia8_png(texture_name, texture_data)
                elif bit_depth == 16:
                    save_ia16_png(texture_name, texture_data)
                else:
                    raise ValueError(f"Unsupported G_IM_FMT_IA bit depth: {bit_depth}")
            elif format == "G_IM_FMT_RGBA":
                if bit_depth == 16:
                    save_rgba5551_png(texture_name, texture_data)
                elif bit_depth == 32:
                    save_rgba32_png(texture_name, texture_data)
                else:
                    raise ValueError(f"Unsupported G_IM_FMT_RGBA bit depth: {bit_depth}")
            elif format == "G_IM_FMT_CI":
                save_ci_png(texture_name, texture_data, palette_data, 8)
            elif format == "G_IM_FMT_I":
                save_i_png(texture_name, texture_data, 8)
            else:
                if width is not None:
                    raise ValueError(f"Unsupported format: {format} in file {file_name}")

        except Exception as exc:
            print(f"Failed to parse {file_name}: {exc}")


def convert_rgba5551_c_file(line_iterator: typing.Iterator[str]) -> None:
    """Converts a C source file RGBA5551 file into PNGs."""
    rgba5551_regex = re.compile(r"unsigned\s+short\s+(\w+)\[\]\s+=\s+{")

    opened_bracket = False
    data = []
    for line in line_iterator:
        match = rgba5551_regex.search(line)
        if match and "}" not in line:
            opened_bracket = True
            matched_file_name = match.groups()[0]
            data = []
        elif opened_bracket:
            if "}" in line:
                opened_bracket = False
                save_rgba5551_png(matched_file_name, data)
            else:
                if "/*" in line or "*/" in line:
                    continue
                if len(line.strip()) == 0:
                    continue
                ints = line.replace(" ", "").replace("0x", "").strip().split(",")
                ints = [i for i in ints if len(i) > 0]
                words = [int(i, 16) for i in ints]
                data.append(words)


def convert_ia_c_file(line_iterator: typing.Iterator[str], bits_per_texel: int) -> None:
    """Converts a C source file IA file into PNGs."""
    if bits_per_texel == 4:
        unsigned_char_data = _get_unsigned_char_data(line_iterator)
        assert unsigned_char_data is not None, f"In convert_ia_c_file: No char array found"
        file_name, chunks = unsigned_char_data
        save_ia4_png(file_name, chunks)
    elif bits_per_texel == 8:
        unsigned_char_data = _get_unsigned_char_data(line_iterator)
        assert unsigned_char_data is not None, f"In convert_ia_c_file: No char array found"
        file_name, chunks = unsigned_char_data
        save_ia8_png(file_name, chunks)
    elif bits_per_texel == 16:
        unsigned_short_data = _get_unsigned_short_data(line_iterator)
        assert unsigned_short_data is not None, f"In convert_ia_c_file: No short array found"
        file_name, chunks = unsigned_short_data
        save_ia16_png(file_name, chunks)
    else:
        raise NotImplementedError(
            f"convert_ia_c_file bits_per_texel={bits_per_texel} not yet supported"
        )


def convert_i_c_file(line_iterator: typing.Iterator[str], bits_per_texel: int) -> None:
    """Converts a C source file intensity file into PNGs."""
    if bits_per_texel in (4, 8):
        unsigned_char_data = _get_unsigned_char_data(line_iterator)
        assert unsigned_char_data is not None, f"In convert_i_c_file: No char array found"
        file_name, chunks = unsigned_char_data
        save_i_png(file_name, chunks, bits_per_texel)
    else:
        raise NotImplementedError(
            f"convert_i_c_file bits_per_texel={bits_per_texel} not yet supported"
        )


def convert_a_c_file(line_iterator: typing.Iterator[str], bits_per_texel: int) -> None:
    """Converts a C source file A file into PNGs."""
    # I don't know what an A file (G_IM_FMT_A) is, so this is a guess, but
    # guessing from the ASCII preview, I think it's 0 == alpha, anything else
    # is intensity?
    assert bits_per_texel == 4, f"Bad bits_per_texel {bits_per_texel} in convert_a_c_file"

    def convert(value: int) -> int:
        if value == 0:
            return 0x00  # 100% alpha
        return (value << 4) | 0x0F

    file_name, chunks = _get_unsigned_char_data(line_iterator)
    formatted_chunks = []
    for row in chunks:
        formatted_row = []
        for value in row:
            formatted_row.append(convert((value & 0xF0) >> 4))
            formatted_row.append(convert(value & 0x0F))
        formatted_chunks.append(formatted_row)

    save_ia8_png(file_name, formatted_chunks)


RGB2C_TYPE_REGEX = re.compile("Format of texel: (\w+)")
RGB2C_BITS_PER_TEXEL_REGEX = re.compile("Number of bits per texel: (\d+)")
def convert_rgb2c_c_file(file_name: str, iterator: typing.Iterator[str]) -> None:
    """Converts a C source file that was produced by rgb2c into a textures."""
    bits_per_texel = 0
    try:
        # We need to read the file format to determine which type it is
        found_type = False
        for line in iterator:
            # Bits per texel comes before format, so we need to grab and save it first
            match = RGB2C_BITS_PER_TEXEL_REGEX.search(line)
            if match:
                bits_per_texel = int(match.groups()[0])

            # Check file format
            match = RGB2C_TYPE_REGEX.search(line)
            if not match:
                continue
            file_format = match.groups()[0]
            if file_format == "G_IM_FMT_RGBA":
                convert_rgba5551_c_file(iterator)
            elif file_format == "G_IM_FMT_IA":
                convert_ia_c_file(iterator, bits_per_texel)
            elif file_format == "G_IM_FMT_A":
                convert_a_c_file(iterator, bits_per_texel)
            elif file_format == "G_IM_FMT_I":
                convert_i_c_file(iterator, bits_per_texel)
            elif file_format == "G_IM_FMT_CI":
                convert_ci_c_file(iterator, bits_per_texel)
            else:
                print(f"Unsupported image format: '{file_format}'")

    except Exception as exc:
        print(f"Failed to parse {file_name}: {exc}")


def _get_unsigned_char_data(
    line_iterator: typing.Iterator[str]
) -> typing.Optional[typing.Tuple[str, typing.List[typing.List[int]]]]:
    """Returns unsigned char data from an array."""
    unsigned_char_re = re.compile(r"unsigned\s+char\s+(\w+)\[\]\s+=\s+{")
    return _get_unsigned_array_data(line_iterator, unsigned_char_re)


def _get_unsigned_short_data(
    line_iterator: typing.Iterator[str]
) -> typing.Optional[typing.Tuple[str, typing.List[typing.List[int]]]]:
    """Returns unsigned short data from an array."""
    unsigned_short_re = re.compile(r"unsigned\s+short\s+(\w+)\[\]\s+=\s+{")
    return _get_unsigned_array_data(line_iterator, unsigned_short_re)


def _get_unsigned_array_data(
    line_iterator: typing.Iterator[str], start_array_regex
) -> typing.Optional[typing.Tuple[str, typing.List[typing.List[int]]]]:
    """Returns unsigned integer data from an array."""
    opened_bracket = False
    data = []
    for line in line_iterator:
        match = start_array_regex.search(line)
        if match and "}" not in line:
            opened_bracket = True
            matched_file_name = match.groups()[0]
            data = []
        elif opened_bracket:
            if "}" in line:
                return matched_file_name, data
            else:
                if "/*" in line or "*/" in line or "#if" in line or "#endif" in line:
                    continue
                if len(line.strip()) == 0:
                    continue
                ints = line.replace(" ", "").replace("0x", "").strip().split(",")
                ints = [i for i in ints if len(i) > 0]
                words = [int(i, 16) for i in ints]
                data.append(words)


def _get_unsigned_data(
    line_iterator: typing.Iterator[str]
) -> typing.Optional[typing.List[typing.List[int]]]:
    """Returns unsigned data from a bare comma-separated list."""
    # f0x just has some entries that are just hex integer data in a list, not
    # in array. See IMAGES/smoke.tex. So whatever, just return that crap.
    data = []
    found_data = False
    for line in line_iterator:
        if found_data:
            if "0x" not in line:
                return data
        if "0x" in line:
            found_data = True
            ints = line.replace(" ", "").replace("0x", "").strip().split(",")
            ints = [i for i in ints if len(i) > 0]
            words = [int(i, 16) for i in ints]
            data.append(words)


def convert_ci_c_file(line_iterator: typing.Iterator[str], bits_per_texel: int) -> None:
    """Converts a C source file CI index file into PNGs."""
    # CI is a list of indexes into a separate palette
    indexes_re = re.compile(r"unsigned\s+char\s+(\w+)\[\]\s+=\s+{")
    palette_re = re.compile(r"unsigned\s+short\s+(\w+)\[\]\s+=\s+{")

    opened_bracket = False
    indexes = []
    palette_colors = []

    # I used to have a 3rd state for looking for bits, which is why I have this
    # instead of a bool
    @enum.unique
    class CiState(enum.Enum):
        LOOKING_FOR_INDEX = 2
        LOOKING_FOR_PALETTE = 3
    state = CiState.LOOKING_FOR_INDEX

    for line in line_iterator:
        if state == CiState.LOOKING_FOR_INDEX:
            match = indexes_re.search(line)
            if match and "}" not in line:
                opened_bracket = True
                file_name = match.groups()[0]
                indexes = []
            elif opened_bracket:
                if "}" in line:
                    state = CiState.LOOKING_FOR_PALETTE
                else:
                    if "/*" in line or "*/" in line:
                        continue
                    if len(line.strip()) == 0:
                        continue
                    ints = line.replace(" ", "").replace("0x", "").strip().split(",")
                    ints = [i for i in ints if len(i) > 0]
                    words = [int(i, 16) for i in ints]
                    if bits_per_texel == 8:
                        indexes.append(words)
                    elif bits_per_texel == 4:
                        words_2 = []
                        for word in words:
                            words_2.append((word & 0xF0) >> 4)
                            words_2.append(word & 0x0F)
                        indexes.append(words_2)
                    else:
                        assert False, f"Bad bits per texel {bits_per_texel}"

        elif state == CiState.LOOKING_FOR_PALETTE:
            match = palette_re.search(line)
            if match and "}" not in line:
                palette_colors = []
            elif opened_bracket:
                if "}" in line:
                    # I don't know if any file has multiple images per file, so
                    # this might be unnecessary. I haven't seen any. But if
                    # there are any, this will need work. Also, if there are
                    # multiples, do they share palettes? Or bit depth?
                    state = CiState.LOOKING_FOR_INDEX
                    save_ci_png(file_name, indexes, palette_colors, 8)
                else:
                    if "/*" in line or "*/" in line:
                        continue
                    if len(line.strip()) == 0:
                        continue
                    ints = line.replace(" ", "").replace("0x", "").strip().split(",")
                    ints = [i for i in ints if len(i) > 0]
                    words = [int(i, 16) for i in ints]
                    palette_colors += words

        else:
            assert False, "Bad state {state}"


def byte_to_caca(word: int) -> typing.List[int]:
    """Converts an IA4 to 4-tuple."""
    return [
        (word & 0b1100_0000),
        (word & 0b0011_0000) << 2,
        (word & 0b0000_1100) << 4,
        (word & 0b0000_0011) << 6,
    ]


def byte_to_ca(word: int) -> typing.List[int]:
    """Converts an IA8 to 2-tuple."""
    return [
        (word & 0b1111_0000),
        (word & 0b0000_1111) << 4,
    ]


def word_to_ca(word: int) -> typing.List[int]:
    """Converts an IA16 to 2-tuple."""
    return [
        (word & 0b1111_1111_0000_0000) >> 16,
        (word & 0b0000_0000_1111_1111),
    ]


def save_ia4_png(
    file_name: str,
    data: typing.List[typing.List[int]],
) -> None:
    """Saves IA4 data to a PNG."""
    # Each entry is a 2 4-bit CCAACCAA
    converted = []
    for row in data:
        converted_row = []
        for byte in row:
            for item in byte_to_caca(byte):
                converted_row.append(item)
        converted.append(converted_row)

    width = len(converted[0]) // 2
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=True, alpha=True)


def save_ia8_png(
    file_name: str,
    data: typing.List[typing.List[int]],
) -> None:
    """Saves IA8 data to a PNG."""
    # Each entry is an 8-bit CCCCAAAA
    converted = []
    for row in data:
        converted_row = []
        for byte in row:
            for item in byte_to_ca(byte):
                converted_row.append(item)
        converted.append(converted_row)

    width = len(converted[0]) // 2
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=True, alpha=True)


def save_ia16_png(
    file_name: str,
    data: typing.List[typing.List[int]],
) -> None:
    """Saves IA16 data to a PNG."""
    # Each entry is 16-bit, 8 for I and 8 for A
    converted = []
    for row in data:
        converted_row = []
        for byte in row:
            for item in word_to_ca(byte):
                converted_row.append(item)
        converted.append(converted_row)

    width = len(converted[0]) // 2
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=True, alpha=True)


def save_i_png(
    file_name: str,
    data: typing.List[typing.List[int]],
    bits_per_texel: int,
) -> None:
    """Saves intensity data to a PNG."""
    converted = []
    for row in data:
        converted_row = []
        for byte in row:
            if bits_per_texel == 8:
                converted_row.append(byte)
            elif bits_per_texel == 4:
                converted_row.append((byte & 0xF0) >> 4)
                converted_row.append(byte & 0x0F)
            else:
                assert False, f"Bad bits per texel {bits_per_texel}"

        converted.append(converted_row)

    width = len(converted[0])
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=True, alpha=False)


def word_to_rgb(word: int) -> typing.List[int]:
    """Converts an RGBA5551 to 4-tuple."""
    part = (
        (word & 0b11111_00000_00000_0) >> 11,
        (word & 0b00000_11111_00000_0) >> 6,
        (word & 0b00000_00000_11111_0) >> 1,
        (word & 0b00000_00000_00000_1) >> 0,
    )
    return [
        int(i * 255. / 0b11111)
        for i in part[:3]
    ] + [int(part[3] * 255)]


def save_rgba5551_png(
    file_name: str,
    data: typing.List[typing.List[int]],
) -> None:
    """Saves RGBA5551 data to a PNG."""

    # Each entry is a 16-bit word RGBA5551
    converted = []
    for row in data:
        converted_row = []
        for word in row:
            for item in word_to_rgb(word):
                converted_row.append(item)
        converted.append(converted_row)

    width = len(converted[0]) // 4
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=False, alpha=True)


def dword_to_rgb(dword: int) -> typing.List[int]:
    """Converts an RGBA4444 dword to 4-tuple."""
    return [
        (dword & 0xFF00_0000) >> 24,
        (dword & 0x00FF_0000) >> 16,
        (dword & 0x0000_FF00) >> 8,
        (dword & 0x0000_00FF) >> 0,
    ]


def save_rgba32_png(
    file_name: str,
    data: typing.List[typing.List[int]],
) -> None:
    """Saves RGBA32 data to a PNG."""

    # Each entry is a 32-bit RGBA8888 dword
    converted = []
    for row in data:
        converted_row = []
        for dword in row:
            for item in dword_to_rgb(dword):
                converted_row.append(item)
        converted.append(converted_row)

    width = len(converted[0]) // 4
    height = len(converted)
    save_png(file_name, width, height, converted, greyscale=False, alpha=True)


def save_ci_png(
    file_name: str,
    indexes: typing.List[int],
    palette_colors: typing.List[int],
    bits_per_texel: int,
) -> None:
    """Saves CI data to a PNG."""

    cached_colors = []
    for value in palette_colors:
        cached_colors.append(word_to_rgb(value))

    converted = []
    if bits_per_texel == 8:
        for row in indexes:
            converted_row = []
            for index in row:
                converted_row += cached_colors[index]
            converted.append(converted_row)
    elif bits_per_texel == 4:
        for row in indexes:
            converted_row = []
            for index in row:
                converted_row += cached_colors[(index & 0xF0) >> 4]
                converted_row += cached_colors[(index & 0x0F)]
            converted.append(converted_row)
    else:
        assert False, f"In save_ci_png: Unsupported bits_per_texel: {bits_per_texel}"

    width = len(converted[0]) // 4
    height = len(converted)

    save_png(file_name, width, height, converted, greyscale=False, alpha=True)


def save_png(
    base_file_name: str,
    width: int,
    height: int,
    data: typing.List[typing.List[int]],
    *,
    greyscale: bool,
    alpha: bool
) -> None:
    """Saves a PNG file, as long as a duplicate file
    hasn't already been saved.
    """
    # So, forest has 100s of duplicate images. Let's just exclude them.
    bytes_io = io.BytesIO()
    writer = png.Writer(width, height, greyscale=greyscale, alpha=alpha)
    writer.write(bytes_io, data)
    hasher = hashlib.new("md5")
    hasher.update(bytes_io.getvalue())
    digest = hasher.digest()
    global ALREADY_SAVED_MD5S
    if digest in ALREADY_SAVED_MD5S:
        return
    ALREADY_SAVED_MD5S.add(digest)

    # If the file already exists, check the MD5. If it's a duplicate, we're
    # done. Otherwise, increment the file name.
    def file_name_iterator() -> str:
        yield f"{OUTPUT_DIRECTORY}{os.sep}{base_file_name}.png"
        i = 1
        while True:
            yield f"{OUTPUT_DIRECTORY}{os.sep}{base_file_name}-{i}.png"
            i += 1

    for file_name in file_name_iterator():
        try:
            hasher = hashlib.new("md5")
            with open(file_name, "rb") as file:
                hasher.update(file.read())
            existing_digest = hasher.digest()
            if existing_digest == digest:
                # File already saved
                return
        except FileNotFoundError:
            break

    with open(file_name, "wb") as file:
        writer = png.Writer(width, height, greyscale=greyscale, alpha=alpha)
        writer.write(file, data)


GAME_DIRECTORY_TO_CONVERTER = {
    "dm64": convert_dm64,
    "f0x": convert_f0x,
    "forest": convert_forest,
    "sm64": convert_sm64,
    "wr64": convert_wr64,
}


if __name__ == "__main__":
    if len(sys.argv) == 2:
        convert_file(sys.argv[1])
    else:
        convert_all_files()

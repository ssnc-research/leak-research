# Super Mario All-Stars (SNES)

Files are in `other.7z/SFC/ソースデータ/srd13-SFCマリオコレクション/export/mario-z/linkp/`, with others scattered around the place too(?).

The `linkp/` folder appears to contain partial assembly source code for Super Mario All-Stars.
SMB1 seems to be mostly complete, and also mostly consists of 1985 NES source, with minor changes to fit the SNES. See the copyright header in some of the files:

```
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;	 1985. 8.12	    V.1.0  Super Mario
;				   File	 name : SMINIT
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

Matching various snippets of this file up with the final NES ROM yields a near-perfect match, although I haven't done any thorough investigation. We currently can't compile any of these files, given we don't have the `as65c` assembler needed (see [as65_notes](/tools/as65c_notes.txt)).

The source for SMB2 (as in, Lost Levels) also seems pretty complete, mostly being a copy of the SMB1 code (which is fair, given they ran on the same engine originally, too).

The SMB3 source code is almost completely missing, with a couple ASM files left over, mostly containing game data, and there's no SMW code either.

The directory `mario_n0` contains mostly build files/the title screen, and refers to a lot of files we don't have.

More info TODO.
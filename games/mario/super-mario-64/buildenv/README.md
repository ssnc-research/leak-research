# Super Mario 64 (N64) - BuildEnv

Files are in `bbgames.7z/sm64/buildenv`.

Contains the libultra library and the tool `makemask` to apply the CIC security code.

## Readable files
 Filename | Content 
----------|---------

## Other important files
The `libultra_rom.a` contains the libultra library required for the game to be run on the Nintendo 64 hardware. `makemask` is a tool that applies the CIC security code to the ROM in order for it to work on the console.

## Complete file list
```
libultra_rom.a
makemask
```
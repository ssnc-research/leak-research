# Super Mario 64 (N64) - Audio

Files are in `bbgames.7z/sm64/audio`.

Contains the code for playing and the audio files. Most of the content is still unknown.

## Readable files
 Filename | Content 
----------|---------
audio-music.h|Unknown
main_inc.h|Unknown

## Other important files
All of the `.cart` files (`mario.banks.cart`, `mario.music.cart`, `mario.table.cart`) probably contain the music data. However, nobody succeeded at extracting those yet.

## Complete file list
```
aspMain.o
audio.o
audio_hb.o
audio_hm.o
audio_hs.o
audio_hw.o
audio_music.h
audioheap.o
main_inc.h
mario.banks.cart
mario.music.cart
mario.table.cart
```